#include <Python.h>
//Here must be include port.h
//#include "port.h"

static PyObject* helloworld(PyObject* self)
{
    return Py_BuildValue("s", "Hello, Python extensions!!");
}

static PyObject *port_add(PyObject *self, PyObject *args) {
   int a;
   int b;

   if (!PyArg_ParseTuple(args, "ii", &a, &b)) {
      return NULL;
   }
   return Py_BuildValue("i", a + b);
}

static PyObject *py_rtk_port_init(PyObject *self, PyObject *args) {   
   int a;

   if (!PyArg_ParseTuple(args, "i", &a)) {
      return NULL;
   }
   int b = rtk_port_init(a);
   return Py_BuildValue("i", b);
}

static char port_docs[] =
    "helloworld( ): Any message you want to put here!!\n";

static PyMethodDef portal_funcs[] = {
    {"rtk_port_init", (PyCFunction)py_rtk_port_init, 
     METH_VARARGS, NULL},
    {"helloworld", (PyCFunction)helloworld, 
     METH_NOARGS, port_docs},
    {"add", (PyCFunction)port_add, 
     METH_VARARGS, port_docs},
    {NULL}
};

void initportal(void)
{
    Py_InitModule3("portal", portal_funcs,
                   "Extension module example!");
}
